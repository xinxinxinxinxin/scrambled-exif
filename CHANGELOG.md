# Changelog
All notable changes to this project will be documented in this file. Format inspired by http://keepachangelog.com/ and this example https://github.com/olivierlacan/keep-a-changelog/blob/master/CHANGELOG.md

## [1.0.2] - 2018-02-03

### Fixed

- Crash if (mistakingly) tapped twice on 'Scrambled Exif' while sharing.


## [1.0.1b] - 2018-01-31

### Changed

- Instructions and

### Updated

- Spanish and German translations

## [1.0.1a] - 2018-01-30

### Added

- Spanish and German translations

### Fixed

- Some minor (lint) issues

## [1.0.1] - 2018-01-28

### Fixed

- Better instructions in main screen

## [1.0] - 2018-01-27

### Added

- First version of Scrambled Eggs
